var nanocode = {
	str_in:function(s, i, u)	{
		if(i == u) return '';

		var n;
		var t = (n = s[ i++ ]) != '\\'? n: (i != u? s[ i++ ]: '');

		if(i < u)	{
			do t += (n = s[ i++ ]) != '\\'?
					n
					: (i != u? s[ i++ ]: '');

			while(i != u)
		}

		return t;
	},

	execCode:function(s)	{
		s = s.split(';');

		var l = s.length,
			p = 0;

		do	{
			var v;
			if((v = this.execFunc( s[ p++ ], p )) != null)
				p = v;
		}
		while(p != l)		
	},


	ifs:[],
	ifsCount:0,
	vars:{},
	namespaces:{},

	execFunc:function(s, q)	{
		var l = s.length,
			p = s.indexOf(':'),
			func = s.substring(0, p++);

		if(q != null)	{
			if(this.ifsCount != 0 &&
				this.ifs[ this.ifsCount -1 ] == false &&
				func != 'else' &&
				func != 'elseif' &&
				func != 'butif' &&
				func != 'fi')
				return null;

			else switch(func)	{
					case 'namespace':
						this.namespaces[ s.substring(p) ] = q;
						return null;
						break;

					case 'gotofi':
						this.ifsCount--;
						this.ifs.pop();

					case 'goto':
						var f;
						if((f = this.namespaces[ s.substring(p) ]) != null)					
							return f;

						else throw alert('A namespace "'+s.substring(p)+'" does not exist');
						break;

					case 'gotofa':
						this.ifsCount = 0;
						this.ifs = [];

						var f;
						if((f = this.namespaces[ s.substring(p) ]) != null)					
							return f;

						else throw alert('A namespace "'+s.substring(p)+'" does not exist');
						break;
					}
		}


		// console.log('func',s);


		var iFunc = 0,
			a = [],
			b = p;

		if(p < l)	{
			do switch(s[ p++ ])	{
			case '\\':
				p += 2;
				break;

			case ':':
				iFunc++;
				break;

			case ',':
				iFunc == 0 &&
					a.push( this.str_in(s, b, (b = p) -1) );
				break;

			case '.':
				iFunc-- == 1 &&
					a.push( this.execFunc( s.substring(b, (b = p) -1) ) );
				break;

			case '_':
				if((iFunc -= cl = parseInt( s.substr(p, 2) )) == 1)	{
					a.push( this.execFunc( s.substring(b, (b = p) -1) ) );
					b += 2;
				}
				p += 2;
				break;
			}
			while(p < l)
		}

		a.push( iFunc != 0?
					this.execFunc( s.substring(b, p) )
					: this.str_in(s, b, p) );


		//console.log(func+'('+a.join(',')+')');
		//return func+'('+a.join(',')+')';

		if(q == null) return this.funcs(func, a);
		else this.funcs(func, a);
	},

	funcs:function(f,a)	{
		console.log(f,a);

		switch(f)	{
		case 'int':
			return parseInt(a[0]);
			break;

		case 'float':
			return parseFloat(a[0]);	
			break;

		case 'string':
			return a[0];	
			break;

		// operators

		case 'sum':
			return a[0] + a[1];	
			break;

		case 'prod':
			return a[0] * a[1];	
			break;

		case 'dif':
			return a[0] - a[1];	
			break;

		case 'divi':
			return a[0] / a[1];	
			break;

		case 'and':
			return a[0] && a[1];	
			break;

		case 'or':
			return a[0] || a[1];	
			break;

		case 'equal':
			return a[0] == a[1];	
			break;

		case 'nequal':
			return a[0] != a[1];	
			break;

		case 'smaller':
			return a[0] < a[1];	
			break;

		case 'bigger':
			return a[0] > a[1];	
			break;

		case 'smallereq':
			return a[0] <= a[1];	
			break;

		case 'biggereq':
			return a[0] >= a[1];	
			break;



		// constants

		case 'true':
			return true;	
			break;

		case 'false':
			return false;	
			break;

		case 'null':
			return null;	
			break;



		// vars

		case 'set':
			return this.vars[ a[0] ] = a[1];	
			break;

		case 'S':
			return this.vars[ a[0] ];	
			break;


		// ifs

		case 'if':
			this.ifs[ this.ifsCount++ ] = a[0] == true;	
			break;

		case 'butif':
			return this.ifs[ this.ifsCount -1 ] = a[0] == true;
			break;

		case 'else':
			var x = this.ifsCount -1,
				a = this.ifs;

			return a[ x ] = a[ x ] == false;
			break;

		case 'elseif':
			var x = this.ifsCount -1,
				a = this.ifs;

			return a[ x ] = a[ x ] == false
								?	a[0] == true
								:	false;
			break;


		case 'fi':
			this.ifsCount--;
			this.ifs.pop();
			break;


		// other funcs

		case 'alert':
			return alert(a[0]);	
			break;
		}
	}
};


nanocode.execCode('set:hallo,3;set:hallo,dif:S:hallo.1;alert:sum:S:hallo.int:4;namespace:xy;set:hallo,prod:int:2.S:hallo;alert:sum:string:Der aktuelle Wert ist .S:hallo;goto:xy;');

// example:
// nanocode.execCode('set:hallo,int:3;set:hallo,dif:S:hallo.1;alert:sum:S:hallo.int:4');
// nanocode.execCode('set:hallo,3;set:hallo,dif:S:hallo.1;alert:sum:S:hallo.int:4;namespace:xy;set:hallo,prod:int:2.S:hallo;alert:sum:string:Der aktuelle Wert ist .S:hallo;goto:xy;');





